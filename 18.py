class Wild_Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age
  
class Bear(Wild_Animal):   
    
    def __init__(self, name, last_name,age):
        super().__init__(name, age)
        self.name = name
        self.age = age
        self.last_name = last_name
        
    def food(self):
        print("Мёд")
    
    def voice(self):
        print("Рев")
        
    def __str__(self):
        return f"Медведю {self.name} всего {self.age} лет"

bear = Bear("Михаил","Потапович", 5)
print(bear)
bear.food()
bear.voice()
