from time import time
def dec_time(func):
    def wrapper(*args, **kwargs):
        t1 = time()
        res = func(*args, **kwargs)
        t2 = time()
        runtime = t2-t1
        print(f'Функция выполняется за {(runtime):.4f}s')
        return res
    return wrapper
    
@dec_time
def time_execution(n):
   for i in range(n):
        for j in range(100000):
            i*j
time_execution(5)
