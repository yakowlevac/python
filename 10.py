def tuple_sort(tpl):
    for i in tpl:
        if not isinstance(i, int):
            return tpl
    return tuple(sorted(tpl))

sort = (23, 78, 2, 0, 9, 19)
print(tuple_sort(sort))
