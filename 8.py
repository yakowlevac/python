def reverse_my_list(lst):
    first = lst.pop(0) 
    last = lst.pop(-1)  
    lst.append(first)  
    lst.insert(0, last) 
    return lst
    
list = [1, 2, 3, 4]
print('Список:', list)    
print('Измененный список:', reverse_my_list(list))  
